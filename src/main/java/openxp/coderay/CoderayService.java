package openxp.coderay;

import com.enonic.xp.context.ContextAccessor;
import com.enonic.xp.context.ContextBuilder;
import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.enonic.xp.security.PrincipalKey;
import com.enonic.xp.security.RoleKeys;
import com.enonic.xp.security.User;
import com.enonic.xp.security.acl.AccessControlEntry;
import com.enonic.xp.security.acl.AccessControlList;
import com.enonic.xp.security.acl.Permission;
import com.enonic.xp.security.auth.AuthenticationInfo;
import com.google.common.base.Strings;
import org.jruby.*;
import org.jruby.embed.osgi.OSGiScriptingContainer;
import org.jruby.javasupport.JavaEmbedUtils;
import org.jruby.runtime.builtin.IRubyObject;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

@Component(immediate = true)
public class CoderayService implements ScriptBean{

    OSGiScriptingContainer container;
    private BeanContext context;
    String defaultCodeType = "html";
    private final Logger LOG = LoggerFactory.getLogger(CoderayService.class);

    private static final AccessControlList PERMISSIONS =
            AccessControlList.of(AccessControlEntry.create().principal(PrincipalKey.ofAnonymous()).allow(Permission.READ).build(),
                    AccessControlEntry.create().principal(RoleKeys.EVERYONE).allow(Permission.READ).build(),
                    AccessControlEntry.create().principal(RoleKeys.AUTHENTICATED).allowAll().build(),
                    AccessControlEntry.create().principal(RoleKeys.CONTENT_MANAGER_ADMIN).allowAll().build());


    @Override
    public void initialize( final BeanContext context ){
        runAs(RoleKeys.CONTENT_MANAGER_ADMIN, () -> {
            doInitialize(context);
            return null;
        });
    }

    private void doInitialize(BeanContext context)
            throws Exception
    {
        System.out.println("start CoderayHighlightingHandler");
        this.context = context;
        final Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        container = new OSGiScriptingContainer(bundle);
        container.addToClassPath(bundle);
    }

    String code;
    String codeType;
    String lineNumbers;
    String css;
    String wrap;
    String hint;
    String breakLines;
    String lineNumberAnchors;
    String lineNumberAnchorsPrefix;
    Integer tabWidth;
    String highlightLines;

    String cacheKey;
    String modifiedTime;
    String parentElement;


    public String highlightCode() {

        checkDefaults();

        codeType = codeType!=null?codeType:defaultCodeType;

        try {
            //http://coderay.rubychan.de/doc/CodeRay
            Object coderay = container.runScriptlet("require 'coderay'\nCodeRay");

            Ruby runtime = container.getProvider().getRuntime();

            //http://coderay.rubychan.de/doc/CodeRay/Encoders/HTML.html (html scanner options)
            RubyHash options = RubyHash.newHash(runtime);
            if (!"nil".equals(lineNumbers)){
                options.put(RubySymbol.newSymbol(runtime, "line_numbers"),RubySymbol.newSymbol(runtime,lineNumbers));
            }
            options.put(RubySymbol.newSymbol(runtime, "css"),RubySymbol.newSymbol(runtime,css));
            options.put(RubySymbol.newSymbol(runtime, "tab_width"),tabWidth!=null?tabWidth:10);
            options.put(RubySymbol.newSymbol(runtime, "wrap"),RubySymbol.newSymbol(runtime,wrap));
            options.put(RubySymbol.newSymbol(runtime, "break_lines"),breakLines);
            //options.put(RubySymbol.newSymbol(runtime, "line_number_start"),1);
            //options.put(RubySymbol.newSymbol(runtime, "bold_every"),2);
            if (!Strings.isNullOrEmpty(highlightLines)){
                try {
                    List<String> list = Arrays.asList(highlightLines.split(","));
                    IRubyObject[] iro = new IRubyObject[list.size()];
                    Iterator<String> it = list.iterator();
                    for (int i = 0;i<list.size();i++) {
                        iro[i] = JavaEmbedUtils.javaToRuby(runtime, Integer.parseInt(list.get(i)));
                    }
                    //RubyArray.newArray(runtime,RubyString.newString(runtime, "2"));
                    //Object arrayClass = RubyArray.createArrayClass(runtime);
                    //IRubyObject[] iro = new IRubyObject[]{JavaEmbedUtils.javaToRuby(runtime,1),JavaEmbedUtils.javaToRuby(runtime,2)};
                    options.put(RubySymbol.newSymbol(runtime, "highlight_lines"), RubyArray.newArray(runtime, iro));
                }catch (Exception e){
                    System.out.println(e);
                    options.put(RubySymbol.newSymbol(runtime, "highlight_lines"),"nil");
                }
            }else{
                options.put(RubySymbol.newSymbol(runtime, "highlight_lines"),"nil");
            }

            options.put(RubySymbol.newSymbol(runtime, "hint"),RubySymbol.newSymbol(runtime,hint));

            if ("true".equals(lineNumberAnchors) || "false".equals(lineNumberAnchors)){
                options.put(RubySymbol.newSymbol(runtime, "line_number_anchors"),
                        RubyBoolean.newBoolean(runtime, Boolean.parseBoolean(lineNumberAnchors)));
            }else if ("prefix".equals(lineNumberAnchors) && !Strings.isNullOrEmpty(lineNumberAnchorsPrefix)){
                options.put(RubySymbol.newSymbol(runtime, "line_number_anchors"),lineNumberAnchorsPrefix);
            }

            Object[] scanParams = new Object[3];
            scanParams[0] = code;
            scanParams[1] = codeType;
            scanParams[2] = options;

            Object tokensProxy = container.callMethod(coderay, "scan", scanParams , Object.class);
            String htmlResult = container.callMethod(tokensProxy, "html", options, String.class);

            return htmlResult;

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;

    }

    private void checkDefaults() {
        if (Strings.isNullOrEmpty(code))code = "<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>";
        if (Strings.isNullOrEmpty(codeType))codeType="html";
        if (Strings.isNullOrEmpty(lineNumbers))lineNumbers="nil";
        if (Strings.isNullOrEmpty(css))css="style";
        if (Strings.isNullOrEmpty(wrap))wrap="div";
        if (Strings.isNullOrEmpty(hint))hint="info";
        if (Strings.isNullOrEmpty(breakLines))breakLines="true";
        if (Strings.isNullOrEmpty(lineNumberAnchors))lineNumberAnchors="false";
        if (Strings.isNullOrEmpty(lineNumberAnchorsPrefix))lineNumberAnchorsPrefix="line";
        if (tabWidth==null)tabWidth=4;
        if (Strings.isNullOrEmpty(highlightLines))highlightLines="nil";
    }

    private <T> T runAs( final PrincipalKey role, final Callable<T> runnable )
    {
        final AuthenticationInfo authInfo = AuthenticationInfo.create().principals( role ).user( User.ANONYMOUS ).build();
        return ContextBuilder.from(ContextAccessor.current()).authInfo( authInfo ).build().callWith( runnable );
    }

    public void setParentElement(String parentElement) {
        this.parentElement = parentElement;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public void setHighlightLines(String highlightLines) {
        this.highlightLines = highlightLines;
    }

    public void setTabWidth(Integer tabWidth) {
        this.tabWidth = tabWidth;
    }

    public void setLineNumberAnchorsPrefix(String lineNumberAnchorsPrefix) {
        this.lineNumberAnchorsPrefix = lineNumberAnchorsPrefix;
    }

    public void setLineNumberAnchors(String lineNumberAnchors) {
        this.lineNumberAnchors = lineNumberAnchors;
    }

    public void setBreakLines(String breakLines) {
        this.breakLines = breakLines;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setWrap(String wrap) {
        this.wrap = wrap;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public void setLineNumbers(String lineNumbers) {
        this.lineNumbers = lineNumbers;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
