package openxp.coderay;

import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import org.osgi.service.component.annotations.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component(immediate = true)
public class CodeCacheService implements ScriptBean{

    private static ConcurrentHashMap<String, String> pageCache = new ConcurrentHashMap();

    @Override
    public void initialize(BeanContext context) {}

    public boolean contains(String cacheKey){
        return pageCache.contains(cacheKey);
    }

    public Object get(String cacheKey){
        return pageCache.get(cacheKey);
    }

    public void put(String cacheKey, String value){
        pageCache.put(cacheKey, value);
    }

    public void clear(){
        pageCache.clear();
    }

    public Object removeCacheKey(String cacheKey) {
        System.out.println("Removeid" + cacheKey);
        for (String key : pageCache.keySet()){
            if (key.startsWith(cacheKey)){
                pageCache.remove(key);
                return true;
            }
        }
        return false;
    }

    public boolean containsCacheKey(String cacheKey) {
        for (String key : pageCache.keySet()){
            if (key.startsWith(cacheKey)){
                return true;
            }
        }
        return false;
    }
}